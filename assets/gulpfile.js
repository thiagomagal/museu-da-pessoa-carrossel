const { src, dest, parallel, series, watch } = require('gulp');
const gulp = require('gulp'),
  sass = require('gulp-sass')(require('sass'));
(rename = require('gulp-rename')),
  (csso = require('gulp-csso')),
  (aliases = require('gulp-style-aliases')),
  (concat = require('gulp-concat')),
  (imagemin = require('gulp-imagemin')),
  (webp = require('imagemin-webp')),
  (imageminSvgo = require('imagemin-svgo')),
  (uglify = require('gulp-uglify'));

const sourceSass = ['./src/scss/main.scss'];

function sassTask() {
  return gulp
    .src(sourceSass)
    .pipe(sass())
    .pipe(csso())
    .pipe(rename({ suffix: '.min' }))
    .pipe(dest('../assets/dist/'));
}

function jsTask() {
  return gulp
    .src('./src/js/**/*.js')
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(dest('./dist/js/'));
}
function imagesTask() {
  return gulp
    .src('./src/img/**')
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({ plugins: [{ removeViewBox: false }] }),
      ]),
    )
    .pipe(dest('./dist/img/'));
}
function watchFiles() {
  watch('./src/scss/**/*.scss', parallel(sassTask));
  watch('./src/js/**/*.js', parallel(jsTask));
}

exports.default = parallel(sassTask, jsTask, imagesTask, watchFiles);
exports.build = series(sassTask, jsTask, imagesTask);
