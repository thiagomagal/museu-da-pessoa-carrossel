function nextSlide() {
  var btns = Array.from(document.getElementsByClassName('carousel-nav__link'));
  var active = document.getElementsByClassName('carousel-nav__link active')[0];
  var index = btns.indexOf(active) + 1;
  if (index >= btns.length) index = 0;
  toSlide(index + 1);
}
function prevSlide() {
  var btns = Array.from(document.getElementsByClassName('carousel-nav__link'));
  var active = document.getElementsByClassName('carousel-nav__link active')[0];
  var index = btns.indexOf(active) + 1;
  if (index == 1) index = btns.length + 1;
  toSlide(index - 1);
}
function toSlide(s) {
  var btns = Array.from(document.getElementsByClassName('carousel-nav__link'));
  btns.forEach((btn) => {
    btn.classList.remove('active');
  });
  document.getElementsByClassName('link-' + s)[0].classList.add('active');
  var slides = Array.from(document.getElementsByClassName('carousel-item'));
  slides.forEach((slide) => {
    slide.classList.remove('active');
  });
  document.getElementsByClassName('item-' + s)[0].classList.add('active');
}

document.onkeydown = checkKey;
function checkKey(e) {
  e = e || window.event;
  if (e.keyCode == '37') {
    prevSlide();
  } else if (e.keyCode == '39') {
    nextSlide();
  }
}

function clear() {
  clearInterval(interval);
}

var interval = setInterval(nextSlide, 5000);

document.addEventListener('touchstart', handleTouchStart, false);
document.addEventListener('touchmove', handleTouchMove, false);

var xDown = null;
var yDown = null;

function getTouches(evt) {
  return (
    evt.touches || // browser API
    evt.originalEvent.touches
  ); // jQuery
}

function handleTouchStart(evt) {
  const firstTouch = getTouches(evt)[0];
  xDown = firstTouch.clientX;
  yDown = firstTouch.clientY;
}

function handleTouchMove(evt) {
  if (!xDown || !yDown) {
    return;
  }

  var xUp = evt.touches[0].clientX;
  var yUp = evt.touches[0].clientY;

  var xDiff = xDown - xUp;
  var yDiff = yDown - yUp;

  if (Math.abs(xDiff) > Math.abs(yDiff)) {
    if (xDiff > 0) {
      nextSlide();
    } else {
      prevSlide();
    }
  }
  /* reset values */
  xDown = null;
  yDown = null;
  clear();
}
